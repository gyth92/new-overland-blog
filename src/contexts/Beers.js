import { createContext, useState, useEffect } from "react"

export const BeersContext = createContext();

export const BeersProvider = (props) => {
    const [beers, setBeers] = useState([])

    useEffect(() => {
        const fetchBeers = async () => {
            const res = await fetch('http://localhost:5000/beers')
            const data = await res.json();

            setBeers(data);
        }

        fetchBeers();
    }, [])


    return (
        <BeersContext.Provider value={beers}>
            {props.children}
        </BeersContext.Provider>
    );
}