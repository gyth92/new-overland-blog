import { useState, useEffect } from 'react';
import './App.css';
import WelcomeImage from './assets/WelcomeBackground.jpg';
import { ParallaxBanner, ParallaxProvider } from 'react-scroll-parallax';
// import Data from './assets/mockData';
import Welcome from './components/Welcome';
import CardsContainer from './components/CardsContainer';
import Navbar from './components/Navbar/Navbar';
import { BeersProvider } from './contexts/Beers';

const App = () => {

  const [beers, setBeers] = useState([]);

  useEffect(() => {
    const getBeers = async () => {
      const dataFromServer = await fetchData();
      setBeers(dataFromServer);
    }

    getBeers();
  }, [])

  const fetchData = async () => {
    const res = await fetch('http://localhost:5000/beers');
    const data = await res.json();
    
    return data;
  }
  
  return (
    <div className="App">
      <Navbar />
      <ParallaxProvider>
        <Welcome />
      </ParallaxProvider>
      <BeersProvider>
        {/* Error handling for when no beers exist */}
        {/* {beers.length > 0 ? (<CardsContainer beers={beers} /> ) : ('No Cards To Show')} */}
        <CardsContainer />
      </BeersProvider>
    </div>
  );
};

export default App;
