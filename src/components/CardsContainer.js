import React from 'react';
import './CardsContainer.css';
import Card from './Card';
import { useContext } from 'react';
import { BeersContext } from '../contexts/Beers';

const CardsContainer = (props) => {
  const beers = useContext(BeersContext);

    return (
      <div className='cards-container'>
        <div className='cards-container-on-tap'>
          {beers.map((beer) => (
            <Card key={beer.id} beer={beer} />
          ))}
        </div>
        {/* <div className='cards-container-brewing'>
        </div> */}
      </div>
    );
  }

export default CardsContainer