import './Navbar.css';

const Dropdown = ({dropdownItems}) => {
    
  return (
    <>
        <ul className='dropdown-menu-nonbootstrap'>
            {dropdownItems.map((item, index) => {
                return (
                    <li key={index}>
                        {item.title}
                    </li>
                );
            })}
        </ul>
    </>
  )
}

export default Dropdown