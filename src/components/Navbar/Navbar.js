import { useEffect, useState } from 'react';
import './Navbar.css';
import NavItem from './NavItem';
import { ReactComponent as BellIcon } from './bell.svg';
import { ReactComponent as CogIcon} from './cog.svg';
import { ReactComponent as PlusIcon} from './plus.svg';
import Dropdown from './Dropdown';

const Navbar = (props) => {
    const [dropdown, setDropdown] = useState(false);
    const [dropdownItems, setDropdownItems] = useState([]);

    useEffect(() => {
        const getDropdownItems = async () => {
            const returnedItems = await fetchData();
            setDropdownItems(returnedItems);
        }

        getDropdownItems();
    }, []);

    const fetchData = async () => {
        const res = await fetch('http://localhost:5000/menuItems');
        const data = await res.json();

        return data;
    }

    const onMouseEnter = () => {
        if(window.innerWidth > 960) {
            setDropdown(true)
        } else {
            setDropdown(false);
        }
    }

    const onMouseLeave = () => {
        if(window.innerWidth > 960) {
            setDropdown(false)
        } else {
            setDropdown(false);
        }
    }

    return (
      <nav className="navbar-nonbootstrap">
        <span className='navbar-logo'>Look at this thing I made</span>
        <ul className="navbar-navigation">
            <li className='nav-item'>
                <NavItem icon={<BellIcon />} />
            </li>
            <li className='nav-item'>
                <NavItem icon={<CogIcon />} />
            </li>
            <li className='nav-item' onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
                <NavItem icon={<PlusIcon />} />
                {dropdown && <Dropdown dropdownItems={dropdownItems}/>}
            </li>
        </ul>
      </nav>
    );
  }


export default Navbar