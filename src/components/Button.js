import PropTypes from 'prop-types';

const Button = ({text, backgroundColor, textColor, onClick}) => {
  return (
    <button className='btn' style={{background: backgroundColor, color: textColor}} onClick={onClick}>
        {text}
    </button>
  )
}

Button.defaultProps = {
    color: 'steelblue'
}

Button.prototype = {
    text: PropTypes.string,
    color: PropTypes.string,
    additionalStyle: PropTypes.object,
    onClick: PropTypes.func
}

export default Button