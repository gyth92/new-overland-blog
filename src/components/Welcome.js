import './Welcome.css';
import TodoList from './TodoList/TodoList';
import SearchBar from './SearchBar';
import WeatherWidget from './WeatherWidget';
import { ParallaxBanner } from 'react-scroll-parallax';
import WelcomeImage from '../assets/WelcomeBackground.jpg'

const Welcome = () => {
    return (
      <ParallaxBanner layers={[{ image: WelcomeImage, speed: -17 }]} className="welcome-container aspect-[2/1]">
        <div className='welcome-container'>
          <div className='welcome-info-container'>
            <div className='frosted-glass-container'>
              <TodoList />
            </div>
            <div className='frosted-glass-container'>
              <WeatherWidget />
              <SearchBar />
            </div>
          </div>
        </div>
        </ParallaxBanner>
    );
  }

export default Welcome