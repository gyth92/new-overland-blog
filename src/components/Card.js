import React from 'react';
import './Card.css';

const Card = ({beer}) => {
    return (
      <div className="card zoom">
        <div className="card-body">
            <h5 className="card-title">{beer.name}</h5>
            <p className="card-text">{beer.style}</p>
        </div>
      </div>
    );
  }

export default Card