import { useState, useEffect } from 'react';
import './WeatherWidget.css';
import { WiCloud, WiDayCloudy, WiDaySunny, WiStrongWind, WiShowers, WiSnow, WiFog } from 'react-icons/wi';
import { FaSearch } from 'react-icons/fa';
import { useCookies } from 'react-cookie';

const WeatherWidget = () => {
    const [weather, setWeather] = useState({});
    const [cookies, setCookie] = useCookies();
    const [showWeather, setShowWeather] = useState(false)
    const weatherAPI = 'https://api.openweathermap.org/data/2.5/weather?appid=05b82421c8f78bd52fc97a330b83f43c&units=imperial';
    const geoLocationAPI = 'http://api.openweathermap.org/geo/1.0/direct?limit=1&appid=05b82421c8f78bd52fc97a330b83f43c'

    useEffect(() => {
        if(cookies.userLocation != null) {
            fetchWeather(cookies.userLocation)
            setShowWeather(true);
        } else {
            setShowWeather(false);
        }
    }, [])

    const searchLocation = async(e) => {
        const geo = {};
        if(e.code === 'Enter') {
            const location = e.target.value
            const res = await fetch(geoLocationAPI + `&q=${location}`);
            const data = await res.json();
            
            geo.lat = data[0].lat;
            geo.lon = data[0].lon;
            setShowWeather(true)
            fetchWeather(geo)
        }
    }

    const fetchWeather = async (geoLocation) => {
        const res = await fetch(weatherAPI + `&lat=${geoLocation.lat}&lon=${geoLocation.lon}`)
        const data = await res.json();

        const weatherData = {}
        if(data) {
            weatherData.location = data.name;
            weatherData.temp = data.main.temp;
            weatherData.humidity = data.main.humidity;
            weatherData.type = data?.weather[0].main;
        }

        setWeather(weatherData);

        let cookieExp = new Date();
        cookieExp.setDate(cookieExp.getDate() + 1);
        setCookie("userLocation", geoLocation, cookieExp)
    }

    const setWeatherType = (weather) => {
        if(weather.type === "Clouds") {
            return <WiCloud />
        } else if (weather.type === "Party Cloudy") {
            return <WiDayCloudy />
        } else if (weather.type === "Clear") {
            return <WiDaySunny />
        } else if (weather.type === "Rain") {
            return <WiShowers />
        } else if (weather.type === "Snow") {
            return <WiSnow />
        } else if (weather.type === "Windy") {
            return <WiStrongWind />
        } else if (weather.type === "Mist" || weather.type === "Fog") {
            return <WiFog />
        }
    }

  return (
    <div>
        { !showWeather && <div className='location-input-container'>
            <input type='text' placeholder='Enter location...' onKeyUp={searchLocation}/>
            <div className='input-icon'>
                <FaSearch /> 
            </div>
        </div>}
        { showWeather && <div className='weather-container'>
            <div className='weather-icon'>
            {setWeatherType(weather)}
            </div>
            <div className='weather-info'>
                <p>{weather.location}</p>
                <p>Temp: {weather.temp}F</p>
                <p>Humidity: {weather.humidity}%</p>
            </div>
        </div> }
    </div>
  );
}

export default WeatherWidget