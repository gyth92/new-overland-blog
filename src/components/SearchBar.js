import { useState } from 'react';
import './SearchBar.css';
import movieData from '../assets/mockSearchbarData.json';
import { FaSearch } from 'react-icons/fa';

const SearchBar = () => {
    const [movies, setMovies] = useState([]);
    const movieApi = 'https://api.themoviedb.org/3/search/movie?api_key=c3b0b24185996c5278bd281266f73039&page=1';

    const handleSearch = async (e) => {
      const searchedTerm = e.target.value;
      if(searchedTerm.length >= 2) {
        const res = await fetch(movieApi + '&query=' + searchedTerm);  
        const data = await res.json();

        setMovies(data.results);
      }
    };

  return (
    <div className='search'>
      <div className='search-input-container'>
          <input type='text' placeholder='Enter a movie...' onChange={handleSearch}/>
          <div className='search-icon'>
              <FaSearch />
          </div>
      </div>
      { movies.length !== 0 &&(
        <div className='returned-data'>
          {movies.map((movie, index) => {
            return (
              <a href={`https://www.imdb.com/find?q=${movie.title}`} key={index} className='data-item' target='_blank'>
                {movie.title} - {movie.release_date}
              </a>
            );
          })}
        </div>
      )}
    </div>
  )
}

export default SearchBar