import { useState } from "react";
import Button from "../Button";
import '../Button.css';

const AddTask = ({onAdd}) => {
    const [text, setText] = useState('');
    const [day, setDay] = useState('');
    const [reminder, setReminer] = useState('');

    const submitButtonStyles = {
        text: '#fff'
    }

    const onSubmit = (e) => {
        e.preventDefault();

        if(!text) {
            alert('Add some text, dingus');
            return;
        }

        onAdd({text, day, reminder});

        setText('');
        setDay('');
        setReminer(false);
    }
    

  return (
    <form className='add-form' onSubmit={onSubmit}>
        <div className='form-control'>
            <label>Task</label>
            <input type="text" placeholder="Add Task" value={text} onChange={(e) => setText(e.target.value)}></input>
        </div>
        <div className='form-control'>
            <label>Date & Time</label>
            <input type="text" placeholder="Add Day & Time" value={day} onChange={(e) => setDay(e.target.value)}></input>
        </div>
        <div className='form-control form-control-check'>
            <label>Set Reminder</label>
            <input type="checkbox" checked={reminder} value={reminder} onChange={(e) => setReminer(e.currentTarget.checked)}></input>
        </div>
        <div className='form-submit'>
            <Button text={'Save Task'} backgroundColor={'#2C3B23'} textColor={'#fff'} />
        </div>

    </form>
  )
}

export default AddTask