import { useState, useEffect } from "react";
import Tasks from "./Tasks";
import './Todo.css';
import TodoHeader from "./TodoHeader";
import AddTask from "./AddTask";


const TodoList = () => {
    const [tasks, setTasks] = useState([]);
    const [showAddTask, setShowAddTask] = useState(false);

    useEffect(() => {
        const getTasks = async () => {
            const tasksFromServer = await fetchTasks();
            setTasks(tasksFromServer)
        }

        getTasks();
    }, []);

    const fetchTasks = async () => {
        const res = await fetch('http://localhost:5000/tasks');
        const data = await res.json();

        return data;
    };

    const fetchTask = async (id) => {
        const res = await fetch(`http://localhost:5000/tasks/${id}`);
        const data = await res.json();

        return data;
    }

    const addTask = async (task) => {
        const res = await fetch('http://localhost:5000/tasks',
        {
            method: 'POST',
            headers: {
            'Content-type': 'application/json'
            },
            body: JSON.stringify(task)
        })

        const data = await res.json();

        setTasks([...tasks, data])
    }

    const deleteTask = async (id) => {
        await fetch(`http://localhost:5000/tasks/${id}`,
        {
            method: 'DELETE',
        })
        
        setTasks(tasks.filter((tasks) => tasks.id !== id))
    }

    const toggleReminder = async (id) => {
        const taskToToggle = await fetchTask(id);
        const updatedTask = {...taskToToggle, reminder: !taskToToggle.reminder};

        const res = await fetch(`http://localhost:5000/tasks/${id}`,
        {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(updatedTask)
        })

        const data = await res.json();

        setTasks(tasks.map((task) => task.id === id ? {...task, reminder: data.reminder} : task))

    }

  return (
    <div className="tasks-container">
        <TodoHeader onAdd={() => setShowAddTask(!showAddTask)} showAdd={showAddTask}/>
        {showAddTask && <AddTask onAdd={addTask}/>}
       { tasks.length > 0 ? <Tasks tasks={tasks} onDelete={deleteTask} onToggle={toggleReminder}/> : ('No asks to show')}
    </div>
  )
}

export default TodoList