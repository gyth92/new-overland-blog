import PropTypes from 'prop-types';
import Button from "../Button";

const TodoHeader = ({title, onAdd, showAdd}) => {
  return (
    <header className="header"> 
        <h1>
            {title}
        </h1>
        <Button backgroundColor={showAdd ? 'red' : 'green'} text={showAdd ? 'Close' : 'Add'} onClick={onAdd}/>

    </header>
  )
}

TodoHeader.defaultProps = {
    title: 'Todo List'
}

TodoHeader.propTypes = {
    title: PropTypes.string.isRequired
}

export default TodoHeader