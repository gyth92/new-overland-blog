import React from 'react';
import './Todo.css';
import { FaTimes } from 'react-icons/fa';

const Task = ({task, onDelete, onToggle}) => {
  return (
    <div className={`task ${task.reminder ? 'reminder' : ''}`} onDoubleClick={() => onToggle(task.id)}>
        <span>{task.text}<FaTimes style={{color: 'red', cursor: 'pointer'}} onClick={() => {onDelete(task.id)}}/></span>
    </div>
  )
}

export default Task